---
layout: handbook-page-toc
title: "Channel Services Catalog"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
# About the GitLab Partner Service Development Framework

The objective of this framework is to help new or existing GitLab partners develop service offerings around GitLab Software. The contents will help you plan and build a new practice area quickly and act as a valuable resource to help you capture new revenue.  We have included guidance for marketing and selling your services, enabling your service teams, and building client relationships through new and recurring service offerings that support longer-term engagements with your customers. 


# The GitLab Partner Services Offering Review

<img src="/images/channel-service-program/partner-services-virtuous-cycle.png" width="" alt="" title="Partner Services Virtuous Cycle">

To help frame the partner opportunity we would suggest partners think about services in one of four key areas that make up a robust service catalog

1. Strategic Assessments & Planning Services
2. Migration & Implementation Services
3. Training & Enablement Services
4. Optimization & Transformation Services

Offered together these services create a virtuous cycle and create the foundation for an ongoing relationship with customers of those services.


# The GitLab Partner Service Development Framework

<table>
  <tr>
   <td>Define Your Starting Point
   </td>
   <td>Evaluate your target outcomes, identify your business model, and plan your offers.
   </td>
  </tr>
  <tr>
   <td>Build Your Team
   </td>
   <td>Identify required skills, staff your teams, train your resources, and complete certifications. 
   </td>
  </tr>
  <tr>
   <td>Building Your Service Catalog
   </td>
   <td>Setup tools, process and resources to launch your offerings.
   </td>
  </tr>
</table>

<br>
# Define Your Service Practice Starting Point 

In order to accommodate various service business models, the framework supports three different options for incorporating GitLab into your Service portfolio. 

<table>
  <tr>
   <td>Resell
   </td>
   <td>Partners can  leverage and resell <a href="https://about.gitlab.com/services/catalog/">GitLab Professional Service SKUs</a> including both project services as well as training services or engage GitLab Professional Services to deliver custom engagements through your Channel Account Manager (CAM) and Sales Account Leader (SAL). 
   </td>
  </tr>
  <tr>
   <td>Professional Services
   </td>
   <td>Partners can leverage any combination of the following 3 ways to build their professional services practices:


<br>Leverage training and enablement materials and certifications from GitLab to include GitLab into their existing professional services practices.

<br>Leverage the pre-packaged services that GitLab Professional services offers as key guidelines for incorporating GitLab in your overall DevOps service portfolio integrated through unique partner developed solutions and intellectual property. 

<br>Re-label/re-brand the GitLab Professional Services packages and sell them as their own, giving partners the ability to optimally price their services and grow their revenue through tested service delivery models. 


   </td>
  </tr>
  <tr>
   <td>GitLab Education Services Packages (currently available by invitation only)
   </td>
   <td>Using the GitLab Training Service packages can help you build a full education practice through our GitLab Certified Training Partner Certification or expand the professional service offerings to include customer education that will drive adoption and the need for follow-on services within your accounts.  If this is interesting to you, bring it up with your Channel Account Manager.
   </td>
  </tr>
</table>
<br>
## Building Your Services Teams

In order to appropriately position your service practice in the market, GitLab suggests that partners hire marketing professionals, sales teams, and service management. Before starting your service practices you should consult with the team in your company who is responsible for market research, competitive analysis and strategic planning to set business goals and direction for your practice. This will ensure the service design, sales and delivery teams are able to focus on creating customer outcomes that will differentiate your organization in the marketplace. 

Below is a description of the functions and resources you will need to design, sell, and deliver your service offerings: 


<table>
  <tr>
   <td>Service Delivery Phase
   </td>
   <td>Service Delivery Consideration
   </td>
   <td>Team Member Job Descriptions
   </td>
  </tr>
  <tr>
   <td>Vision and Scope Definition
   </td>
   <td>* Help your customer envision scenarios that drive measurable and positive outcomes.
<br>
* Identify the initial or next logical opportunity that can be used to build confidence in your capabilities, services and the solution if you are just beginning or drive additional value for your existing customers.
<br>
* Build a roadmap that outlines a path from the smaller, low risk, near term projects to larger initiatives that deliver greater value.	
   </td>
   <td><a href="https://about.gitlab.com/job-families/sales/strategic-account-leader/">Account Leaders</a>
<br>
<a href="https://about.gitlab.com/job-families/sales/solutions-architect/">Solution Architects</a>
<br>
<a href="https://about.gitlab.com/job-families/sales/job-professional-services-engagement-manager/">Professional Services Engagement Manager</a>
   </td>
  </tr>
  <tr>
   <td>Project Delivery
   </td>
   <td>* Design and implement your initial business application solution with the customer.
<br>
* Delivery professional training curriculum to the customer’s teams to drive user adoption.
<br>
* Capture insights and validate solution/offer design assumptions throughout the process to inform your practice strategy and optimization efforts. 
   </td>
   <td><a href="https://about.gitlab.com/job-families/sales/professional-services-practice-manager/">Professional Service Practice Manager</a>
<br>
<a href="https://about.gitlab.com/job-families/sales/professional-services-project-coordinator/">Professional Services Project Coordinator</a>
<br>
<a href="https://about.gitlab.com/job-families/sales/professional-services-project-manager/">Professional Services Project Manager</a>
<br>
<a href="https://about.gitlab.com/job-families/sales/professional-services-engineer/">Professional Services Engineer</a>
<br>
<a href="https://about.gitlab.com/job-families/sales/professional-services-engineer/#professional-services-technical-instructor">Professional Services Technical Instructor</a>
   </td>
  </tr>
  <tr>
   <td>Building on the Initial Engagement
   </td>
   <td>* Convert the insights to expand your opportunities with the customer. 
<br>
* Offer services to monitor their solution for measurable impacts and benefits, and optimize their solution.
   </td>
   <td><a href="https://about.gitlab.com/job-families/sales/technical-account-manager/">Technical Account Manager</a>
   </td>
  </tr>
</table>
<br>
### Training Technical Resources 

For technical staff to function as change agents supporting current and emerging cloud technologies, their buy-in for the use and integration of these technologies is needed. [GitLab offers various sales, technical sales, and professional services learning paths and certifications to enable your technical team members to gain knowledge and validate their capabilities](https://about.gitlab.com/handbook/resellers/training/).  

After your organization has sponsored technical resources to achieve these certifications, you can become a [GitLab Certified Services Partner](https://about.gitlab.com/handbook/resellers/services/).  To learn more about which certification you need 

## Building Your Service Catalog

<img src="/images/channel-service-program/partner-services-virtuous-cycle.png" width="" alt="" title="Partner Services Virtuous Cycle">


While GitLab fundamentally changes the way Development, Security, and Ops teams collaborate and build software, the challenges that many organizations are facing require not only a class-leading DevSecOps platform but help and services from trusted service providers to maximize the benefits of that platform. 

For simplicity we group those services into a service catalog made up of four key areas which can be delivered or thought of as phases in a virtuous cycle.   

**Note**: while we have numbered the phases to align with projects where partners are beginning their relationship with a customer, there is no fixed starting point in this virtuous cycle.  For your existing customers, it is actually likely that you might be starting elsewhere in the cycle, the key is working to make sure your relationships are leading you towards a continuous level of engagement with your customers so that you can lead them through the cycle over and over again driving incremental value for the customer and solidifying you as their trusted advisor.

### Strategic Assessments & Planning Services

<img src="/images/channel-service-program/assessments-virtuous-cycle.png" width="" alt="" title="Strategic Assessments & Training Services">

   * Potential services 
     * Assessments discovering, outlining, and validating a customers current state and desired future state 
          * DevOps focused
          * GitOps focused
          * Security/compliance focused
          * Comprehensive  - DevSecOps focused 
          * GitLab health checks
          * Tool, application &/or platform rationalization
     * Strategic plans helping customers set measurable goals for, plan and prioritize activities to minimize the cost and effort to achieve desired future state outcomes
   * Business model options - potential services delivered individually or as a bundle, and potentially as a funnel from lower cost services to higher cost services
     * Fixed-price packages
     * Variable-price packages
     * Loss-leader offers delivered either free or at discounted rates

### Migration & Implementation Services


<img src="/images/channel-service-program/implementation-virtuous-cycle.png" width="" alt="" title="Migrations & Implementation Services">

   * Potential services
     * Migrations (examples: GitHub or BitBucket to GitLab; Jenkins to GitLab)
     * Implementations of GitLab (examples: quick start offers; complete VCC (version control & collaboration) or CI implementation)
     * Integrations (example: Jira, other security tools or platforms)
     * Managed or hosted services (examples: managing/operating a customer’s instance of GitLab; ongoing team support or functional expertise) 
   * Business model options
     * Fixed-price packages
     * Variable-price packages
     * Managed services (MSP) model
   * GitLab service kits - practice and offering building assets and templates 
     * Datasheet
          1. [Migrations](https://partners.gitlab.com/prm/English/s/assets?id=232689)
          2. [Quick Start Implementation](https://partners.gitlab.com/prm/English/s/assets?id=232687)
     * SOW & Project Plan
          * [Migrations](https://partners.gitlab.com/prm/English/s/assets?id=232694)
          * [Migrations including reselling GitLab Professional Services Automation](https://partners.gitlab.com/prm/English/s/assets?id=232692)
          * [Quick Start on GitLab.com](https://partners.gitlab.com/prm/English/s/assets?id=232684)
          * [Quick Start - Self Hosted](https://partners.gitlab.com/prm/English/s/assets?id=232685)

### Training & Enablement Services


<img src="/images/channel-service-program/training-virtuous-cycle.png" width="" alt="" title="Training Services">

   * Potential services
     * Training or education
     * Consulting services focused on driving adoption
     * Consulting services focused on establishing, enabling, and measuring success metrics
   * Business model options
     * Fixed-price packages
     * Variable-price packages
     * Managed services (MSP) model - used for ongoing training(learning platform)-as-a-service
   * GitLab service kits - practice and offering building assets and templates 
     * [Datasheet](https://partners.gitlab.com/prm/English/s/assets?id=232697)

### Optimization & Transformation Services 


<img src="/images/channel-service-program/optimization-virtuous-cycle.png" width="" alt="" title="Optimization Services">

   * Potential services
     * Platform and process optimization services
     * Platform and process health checks
     * Metrics analysis and reporting services
     * Executive business review preparation and delivery services (at customer cadence: Monthly, Quarterly, Yearly, other)
   * Business model options
     * Fixed-price packages
     * Variable-price packages
     * Managed services (MSP) model  - The MSP model is uniquely suited to the optimization and transformation phase, essentially enabling a customer to subscribe to a service or set of services that will help the customer ensure that their goals are not only achieved in the short term but also evolved and continuously improved to drive value in real time.
   * GitLab Service Guidance  - comparing your optimization and transformation services to GitLab’s Customer Success motion
     * For enterprise customers with significant GitLab licensing, GitLab provides a Technical Account Manager who helps customers maximize the value they receive from GitLab.  
     * Here is a link to the [GitLab TAM Handbook](https://about.gitlab.com/handbook/customer-success/tam/services/) page where you can see the services they provide which are a great starting point for ideating or differentiating your optimization and transformation services.

### Go to Market (GTM) & Aligning With GitLab GTM

It likely goes without saying that you will need to add any GitLab enabled or related services to your Go to Market strategies and execution.  While your GTM is unique to your offerings, your market, and sales and marketing model, we encourage you to understand and align your GTM with GitLab where applicable.

The foundation for GitLab’s GTM is the flow from Why to What to How.


<img src="/images/channel-service-program/why-usecases-overall.png" width="" alt="" title="GitLab Use Cases">

*   Why is the service or product needed, which from a GitLab perspective is tied to one or more of the following value drivers?
    *   Increase Operational Efficiencies - simplify the software development toolchain to reduce total cost of ownership
    *   Deliver Better Products Faster - accelerate the software delivery process to meet business objectives
    *   Reduce Security and Compliance Risk - simplify processes to comply with internal processes, controls and industry regulations without compromising speed
*   What can the product or service accomplish, which from a GitLab perspective is identified in one of the following primary use cases?
    *   Version Control and Collaboration (VC&C)
    *   Continuous Integration (CI)
    *   Continuous Delivery (CD)
    *   DevSecOps (Shift Left Security)
    *   Agile Management
    *   GitOps
    *   DevOps Platform
    *   Cloud Native
*   How can the service or product meet the needs of the use case(s), which from a GitLab perspective is delivered by the features and functions of our product.

If you want to explore GitLab’s GTM further here are some resources that you might find useful:


*   [GitLab’s Customer Value Drivers](https://about.gitlab.com/handbook/sales/command-of-the-message/#customer-value-drivers)
*   [Why - Use Case Driven Go to Market?](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/#why---use-case-driven-go-to-market)
*   [GitLab Customer Use Cases](https://about.gitlab.com/handbook/use-cases/)
